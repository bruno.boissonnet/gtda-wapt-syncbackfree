# GTDA-WAPT-SyncbackFree

Paquet [WAPT](https://www.tranquil.it/gerer-parc-informatique/decouvrir-wapt/) pour l'installation silencieuse de Global Protect.


## Paquet SyncbackFree

Ce paquet installe la dernière version de 2BrightSparks SynbackFree de manière silencieuse.

Dernière version du logiciel Global Protect: 10.2.68.0

## Utiliser ce paquet

Pour utiliser ce paquet, il faut : 

1. Télécharger tous les fichiers
2. Ouvrir la console WAPT
3. Générer un modèle de paquet
4. Pointer sur les fichiers téléchargés

