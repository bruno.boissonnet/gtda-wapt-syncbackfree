# -*- coding: utf-8 -*-
from setuphelpers import *

r"""
Usable WAPT package functions: install(), uninstall(), session_setup(), audit(), update_package()

"""
# Declaring global variables - Warnings: 1) WAPT context is only available in package functions; 2) Global variables are not persistent between calls


def install():
    # Declaring local variables

    # Installing the software
    print("Installing: SyncBack_Setup.exe")
    install_exe_if_needed('SyncBack_Setup.exe',
        silentflags='/VERYSILENT',
        key='SyncBackFree_is1',
        min_version='10.2.68.0'
    )



